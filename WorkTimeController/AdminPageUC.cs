﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkTimeController
{
    public partial class AdminPageUC : UserControl
    {
        private MainForm formMain = null;
        private AdminPageUC() { }
        public AdminPageUC(MainForm f)
        {
            InitializeComponent();
            formMain = f;
        }
    }
}
