﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkTimeController
{
    public partial class MainPageUC : UserControl
    {
        public MainPageUC()
        {
            InitializeComponent();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            lTime.Text = ((DateTime.Now.Day < 10) ? "0" : "") + DateTime.Now.Day.ToString() + "." + ((DateTime.Now.Month < 10) ? "0" : "") + DateTime.Now.Month.ToString() + "." + DateTime.Now.Year.ToString() + " " + ((DateTime.Now.Hour < 10) ? "0" : "") + DateTime.Now.Hour.ToString() + ":" + ((DateTime.Now.Minute < 10) ? "0" : "") + DateTime.Now.Minute.ToString() + ":" + ((DateTime.Now.Second < 10) ? "0" : "") + DateTime.Now.Second.ToString();
        }

        private void MainPageUC_Load(object sender, EventArgs e)
        {
            Size = new Const().PageSize;
            lTime.Text = ((DateTime.Now.Day < 10) ? "0" : "") + DateTime.Now.Day.ToString() + "." + ((DateTime.Now.Month < 10) ? "0" : "") + DateTime.Now.Month.ToString() + "." + DateTime.Now.Year.ToString() + " " + ((DateTime.Now.Hour < 10) ? "0" : "") + DateTime.Now.Hour.ToString() + ":" + ((DateTime.Now.Minute < 10) ? "0" : "") + DateTime.Now.Minute.ToString() + ":" + ((DateTime.Now.Second < 10) ? "0" : "") + DateTime.Now.Second.ToString();
            lHead.Location = new System.Drawing.Point((int)((Size.Width / 2) - (lHead.Size.Width / 2)), 0);
            lTime.Location = new System.Drawing.Point((int)((Size.Width / 2) - (lTime.Size.Width / 2)), (int)(Size.Height - lTime.Height * 1.5));

            timeWorkControllerDataSetTableAdapters.TableAdapterManager tableAdapterManager = new timeWorkControllerDataSetTableAdapters.TableAdapterManager();
            timeWorkControllerDataSetTableAdapters.ПроходTableAdapter проходTableAdapter = new timeWorkControllerDataSetTableAdapters.ПроходTableAdapter();
            timeWorkControllerDataSet.ПроходDataTable проходDataTable = проходTableAdapter.GetData();

            tableAdapterManager.ПроходTableAdapter = проходTableAdapter;

            int countLogin = 0, countExit = 0;
            string t = ((DateTime.Now.Day < 10) ? "0" : "") + DateTime.Now.Day.ToString() + "." + ((DateTime.Now.Month < 10) ? "0" : "") + DateTime.Now.Month.ToString() + "." + ((DateTime.Now.Year < 10) ? "0" : "") + DateTime.Now.Year.ToString();
            for (int i = 0; i < проходDataTable.Count; ++i)
            {
                if (проходDataTable.Rows[i][1].ToString().StartsWith(t))
                {
                    if (проходDataTable.Rows[i][2].ToString() == "Вошел")
                        countLogin++;
                    else countExit++;
                }
            }
            lItem1.Text = countLogin.ToString();
            lItem2.Text = countExit.ToString();

        }
    }
}
