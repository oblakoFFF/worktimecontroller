﻿namespace WorkTimeController
{
    partial class MainPageUC
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lHead = new System.Windows.Forms.Label();
            this.lTime = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.lLine1 = new System.Windows.Forms.Label();
            this.lItem1 = new System.Windows.Forms.Label();
            this.lItem2 = new System.Windows.Forms.Label();
            this.lLine2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lHead
            // 
            this.lHead.AutoSize = true;
            this.lHead.Font = new System.Drawing.Font("Arial Black", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lHead.Location = new System.Drawing.Point(32, 0);
            this.lHead.Name = "lHead";
            this.lHead.Size = new System.Drawing.Size(461, 68);
            this.lHead.TabIndex = 0;
            this.lHead.Text = "— Статистика —";
            // 
            // lTime
            // 
            this.lTime.AutoSize = true;
            this.lTime.Font = new System.Drawing.Font("Arial Black", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lTime.Location = new System.Drawing.Point(228, 555);
            this.lTime.Name = "lTime";
            this.lTime.Size = new System.Drawing.Size(74, 33);
            this.lTime.TabIndex = 1;
            this.lTime.Text = "time";
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // lLine1
            // 
            this.lLine1.AutoSize = true;
            this.lLine1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lLine1.Location = new System.Drawing.Point(41, 91);
            this.lLine1.Name = "lLine1";
            this.lLine1.Size = new System.Drawing.Size(206, 19);
            this.lLine1.TabIndex = 2;
            this.lLine1.Text = "Кол-во входов на сегодня:";
            // 
            // lItem1
            // 
            this.lItem1.AutoSize = true;
            this.lItem1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lItem1.Location = new System.Drawing.Point(264, 91);
            this.lItem1.Name = "lItem1";
            this.lItem1.Size = new System.Drawing.Size(48, 19);
            this.lItem1.TabIndex = 3;
            this.lItem1.Text = "count";
            // 
            // lItem2
            // 
            this.lItem2.AutoSize = true;
            this.lItem2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lItem2.Location = new System.Drawing.Point(264, 119);
            this.lItem2.Name = "lItem2";
            this.lItem2.Size = new System.Drawing.Size(48, 19);
            this.lItem2.TabIndex = 5;
            this.lItem2.Text = "count";
            // 
            // lLine2
            // 
            this.lLine2.AutoSize = true;
            this.lLine2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lLine2.Location = new System.Drawing.Point(41, 119);
            this.lLine2.Name = "lLine2";
            this.lLine2.Size = new System.Drawing.Size(217, 19);
            this.lLine2.TabIndex = 4;
            this.lLine2.Text = "Кол-во выходов на сегодня:";
            // 
            // MainPageUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.lItem2);
            this.Controls.Add(this.lLine2);
            this.Controls.Add(this.lItem1);
            this.Controls.Add(this.lLine1);
            this.Controls.Add(this.lTime);
            this.Controls.Add(this.lHead);
            this.Name = "MainPageUC";
            this.Size = new System.Drawing.Size(525, 608);
            this.Load += new System.EventHandler(this.MainPageUC_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lHead;
        private System.Windows.Forms.Label lTime;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label lLine1;
        private System.Windows.Forms.Label lItem1;
        private System.Windows.Forms.Label lItem2;
        private System.Windows.Forms.Label lLine2;
    }
}
