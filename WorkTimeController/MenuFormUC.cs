﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkTimeController
{
    public partial class MenuFormUC : UserControl
    {
        private MainForm formMain = null;

        private MenuFormUC() { }

        public MenuFormUC(MainForm f)
        {
            InitializeComponent();
            formMain = f;
        }

        private void MenuFormUC_Load(object sender, EventArgs e)
        {
            Size = new Const().MenuSize;
            bMain.Size = new System.Drawing.Size(Size.Width, (int)(Size.Height / 8));

            b2.Size = new System.Drawing.Size(Size.Width, (int)(Size.Height / 8));
            b2.Location = new System.Drawing.Point(0, bMain.Location.Y + bMain.Size.Height);

            b3.Size = new System.Drawing.Size(Size.Width, (int)(Size.Height / 8));
            b3.Location = new System.Drawing.Point(0, b2.Location.Y + b2.Size.Height);

            b4.Visible = false;
            b4.Size = new System.Drawing.Size(Size.Width, (int)(Size.Height / 8));
            b4.Location = new System.Drawing.Point(0, b3.Location.Y + b3.Size.Height);

            b5.Visible = false;
            b5.Size = new System.Drawing.Size(Size.Width, (int)(Size.Height / 8));
            b5.Location = new System.Drawing.Point(0, b4.Location.Y + b4.Size.Height);

            b6.Visible = false;
            b6.Size = new System.Drawing.Size(Size.Width, (int)(Size.Height / 8));
            b6.Location = new System.Drawing.Point(0, b5.Location.Y + b5.Size.Height);

            b7.Visible = false;
            b7.Size = new System.Drawing.Size(Size.Width, (int)(Size.Height / 8));
            b7.Location = new System.Drawing.Point(0, b6.Location.Y + b6.Size.Height);

            bAuth.Size = new System.Drawing.Size(Size.Width, (int)(Size.Height / 8));
            bAuth.Location = new System.Drawing.Point(0, b7.Location.Y + b7.Size.Height);
        }

        private void bMain_Click(object sender, EventArgs e)
        {
            formMain.pagePanel.Controls.Clear();
            formMain.pagePanel.Controls.Add(new MainPageUC());

            //formMain.pagePanel.Parent = new MenuFormUC(formMain) { Parent = formMain.menuPanel };

            //new MenuFormUC(formMain) { Parent = formMain.menuPanel };

            //formMain.mainPageUC1.Visible = true;
            //formMain.adminPageUC1.Visible = false;
            //formMain.authPageUC1.Visible = false;
        }

        private void bAuth_Click(object sender, EventArgs e)
        {
            //formMain.pagePanel.Controls.Clear();
            //formMain.pagePanel.Controls.Add(new AuthPageUC(formMain));

            formMain.pagePanel.Parent = new AuthPageUC(formMain);

            //formMain.Controls.Add(new AuthPageUC(formMain));
            //new AuthPageUC(formMain) { Parent = formMain.pagePanel };
            //formMain.pagePanel.Parent = new AuthPageUC(formMain);

            //formMain.mainPageUC1.Visible = false;
            //formMain.adminPageUC1.Visible = false;
            //formMain.authPageUC1.Visible = true;
        }
    }
}
