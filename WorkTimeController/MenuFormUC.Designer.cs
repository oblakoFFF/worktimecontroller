﻿namespace WorkTimeController
{
    partial class MenuFormUC
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.bMain = new System.Windows.Forms.Button();
            this.b2 = new System.Windows.Forms.Button();
            this.b3 = new System.Windows.Forms.Button();
            this.b4 = new System.Windows.Forms.Button();
            this.b5 = new System.Windows.Forms.Button();
            this.b6 = new System.Windows.Forms.Button();
            this.b7 = new System.Windows.Forms.Button();
            this.bAuth = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bMain
            // 
            this.bMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.bMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 27F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bMain.Location = new System.Drawing.Point(0, 0);
            this.bMain.Name = "bMain";
            this.bMain.Size = new System.Drawing.Size(275, 70);
            this.bMain.TabIndex = 0;
            this.bMain.Text = "Main";
            this.bMain.UseVisualStyleBackColor = false;
            this.bMain.Click += new System.EventHandler(this.bMain_Click);
            // 
            // b2
            // 
            this.b2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.b2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.b2.Location = new System.Drawing.Point(0, 67);
            this.b2.Name = "b2";
            this.b2.Size = new System.Drawing.Size(275, 70);
            this.b2.TabIndex = 1;
            this.b2.Text = "b2";
            this.b2.UseVisualStyleBackColor = false;
            // 
            // b3
            // 
            this.b3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.b3.Font = new System.Drawing.Font("Microsoft Sans Serif", 27F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.b3.Location = new System.Drawing.Point(0, 134);
            this.b3.Name = "b3";
            this.b3.Size = new System.Drawing.Size(275, 70);
            this.b3.TabIndex = 2;
            this.b3.Text = "b3";
            this.b3.UseVisualStyleBackColor = false;
            // 
            // b4
            // 
            this.b4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.b4.Font = new System.Drawing.Font("Microsoft Sans Serif", 27F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.b4.Location = new System.Drawing.Point(0, 201);
            this.b4.Name = "b4";
            this.b4.Size = new System.Drawing.Size(275, 70);
            this.b4.TabIndex = 3;
            this.b4.Text = "b4";
            this.b4.UseVisualStyleBackColor = false;
            // 
            // b5
            // 
            this.b5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.b5.Font = new System.Drawing.Font("Microsoft Sans Serif", 27F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.b5.Location = new System.Drawing.Point(0, 268);
            this.b5.Name = "b5";
            this.b5.Size = new System.Drawing.Size(275, 70);
            this.b5.TabIndex = 4;
            this.b5.Text = "b5";
            this.b5.UseVisualStyleBackColor = false;
            // 
            // b6
            // 
            this.b6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.b6.Font = new System.Drawing.Font("Microsoft Sans Serif", 27F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.b6.Location = new System.Drawing.Point(0, 335);
            this.b6.Name = "b6";
            this.b6.Size = new System.Drawing.Size(275, 70);
            this.b6.TabIndex = 5;
            this.b6.Text = "b6";
            this.b6.UseVisualStyleBackColor = false;
            // 
            // b7
            // 
            this.b7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.b7.Font = new System.Drawing.Font("Microsoft Sans Serif", 27F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.b7.Location = new System.Drawing.Point(0, 402);
            this.b7.Name = "b7";
            this.b7.Size = new System.Drawing.Size(275, 70);
            this.b7.TabIndex = 6;
            this.b7.Text = "b7";
            this.b7.UseVisualStyleBackColor = false;
            // 
            // bAuth
            // 
            this.bAuth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.bAuth.Font = new System.Drawing.Font("Microsoft Sans Serif", 27F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bAuth.Location = new System.Drawing.Point(0, 470);
            this.bAuth.Name = "bAuth";
            this.bAuth.Size = new System.Drawing.Size(275, 70);
            this.bAuth.TabIndex = 7;
            this.bAuth.Text = "Login";
            this.bAuth.UseVisualStyleBackColor = false;
            this.bAuth.Click += new System.EventHandler(this.bAuth_Click);
            // 
            // MenuFormUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Controls.Add(this.bAuth);
            this.Controls.Add(this.b7);
            this.Controls.Add(this.b6);
            this.Controls.Add(this.b5);
            this.Controls.Add(this.b4);
            this.Controls.Add(this.b3);
            this.Controls.Add(this.b2);
            this.Controls.Add(this.bMain);
            this.Name = "MenuFormUC";
            this.Size = new System.Drawing.Size(275, 600);
            this.Load += new System.EventHandler(this.MenuFormUC_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bMain;
        private System.Windows.Forms.Button b2;
        private System.Windows.Forms.Button b3;
        private System.Windows.Forms.Button b4;
        private System.Windows.Forms.Button b5;
        private System.Windows.Forms.Button b6;
        private System.Windows.Forms.Button b7;
        public System.Windows.Forms.Button bAuth;
    }
}
