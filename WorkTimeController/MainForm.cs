﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkTimeController
{
    public partial class MainForm : Form
    {
        public string inAuth = null;
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            new MenuFormUC(this) { Parent = menuPanel };
            //new AuthPageUC(this) { Parent = pagePanel };
            new MainPageUC() { Parent = pagePanel };
            ClientSize = new Const().MainSize;
            menuPanel.Location = new System.Drawing.Point(0, 0);
            menuPanel.Size = new Const().MenuSize;
            pagePanel.Location = new System.Drawing.Point(menuPanel.Location.X + menuPanel.Size.Width, 0);
            pagePanel.Size = new Const().PageSize;

            //new MenuFormUC(this) { Parent = menuFormUC1 };
            //new AuthPageUC(this) { Parent = authPageUC1 };
            //new AdminPageUC(this) { Parent = adminPageUC1 };
            //ClientSize = new Const().MainSize;
            //menuFormUC1.Location = new System.Drawing.Point(0, 0);
            //menuFormUC1.Size = new Const().MenuSize;
            //mainPageUC1.Location = new System.Drawing.Point(menuFormUC1.Location.X + menuFormUC1.Size.Width, 0);
            //mainPageUC1.Size = new Const().PageSize;
            //authPageUC1.Location = new System.Drawing.Point(menuFormUC1.Location.X + menuFormUC1.Size.Width, 0);
            //authPageUC1.Size = new Const().PageSize;
            //adminPageUC1.Location = new System.Drawing.Point(menuFormUC1.Location.X + menuFormUC1.Size.Width, 0);
            //adminPageUC1.Size = new Const().PageSize;
        }
    }
}
