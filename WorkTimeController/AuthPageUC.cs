﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WorkTimeController
{
    public partial class AuthPageUC : UserControl
    {
        private MainForm formMain = null;
        private AuthPageUC() { }
        public AuthPageUC(MainForm f)
        {
            InitializeComponent();
            formMain = f;
        }

        private void AuthPageUC_Load(object sender, EventArgs e)
        {
            Size = new Const().PageSize;
            tbLogin.Text = "Login";
            tbLogin.ForeColor = Color.Gray;
            tbPassword.Text = "Password";
            tbPassword.ForeColor = Color.Gray;

        }

        private void tbLogin_Enter(object sender, EventArgs e)
        {
            if (Equals(tbLogin.Text, "Login")) tbLogin.Text = "";
            tbLogin.ForeColor = System.Drawing.SystemColors.WindowText;
        }
        
        private void tbLogin_Leave(object sender, EventArgs e)
        {
            if (Equals(tbLogin.Text, ""))
            {
                tbLogin.Text = "Login";
                tbLogin.ForeColor = Color.Gray;
            }
        }

        private void tbPassword_Enter(object sender, EventArgs e)
        {
            if (Equals(tbPassword.Text, "Password"))
            {
                tbPassword.Text = "";
            }
            tbPassword.UseSystemPasswordChar = true;
            tbPassword.ForeColor = System.Drawing.SystemColors.WindowText;
        }

        private void tbPassword_Leave(object sender, EventArgs e)
        {
            if (tbPassword.Text.Length == 0)
            {
                tbPassword.UseSystemPasswordChar = false;
                tbPassword.Text = "Password";
                tbPassword.ForeColor = Color.Gray;
            }
        }

        private void bAuth_Click(object sender, EventArgs e)
        {
            timeWorkControllerDataSetTableAdapters.TableAdapterManager tableAdapterManager = new timeWorkControllerDataSetTableAdapters.TableAdapterManager();
            timeWorkControllerDataSetTableAdapters.AdminsTableAdapter adminsTableAdapter = new timeWorkControllerDataSetTableAdapters.AdminsTableAdapter();
            timeWorkControllerDataSet.AdminsDataTable adminsDataTable = adminsTableAdapter.GetData();

            tableAdapterManager.AdminsTableAdapter = adminsTableAdapter;

            for (int i = 0; i < adminsDataTable.Count; ++i)
            {
                if (Equals(adminsDataTable.Rows[i][1], tbLogin.Text) && Equals(adminsDataTable.Rows[i][2], tbPassword.Text))
                {
                    Console.WriteLine("Успех" + tbLogin.Text);
                    //formMain.authPageUC1.Visible = false;
                    //formMain.adminPageUC1.Visible = true;
                    formMain.inAuth = tbLogin.Text;

                    MenuFormUC formMenu = new MenuFormUC(formMain);
                    formMain.Text = tbLogin.Text;
                    //formMenu.Parent = formMain.menuFormUC1;
                    //MenuFormUC formMenu = new MenuFormUC(formMain);
                    //formMenu.bAuth.Text = tbLogin.Text;
                    //formMenu.Parent = formMain.menuFormUC1;
                    break;
                }
            }
        }

    }
}
