﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkTimeController
{
    public class Const
    {
        public Const()
        {
            MainSize = new System.Drawing.Size(800, 600);
            MenuSize = new System.Drawing.Size((int)(800*0.35), 600);
            PageSize = new System.Drawing.Size(MainSize.Width - MenuSize.Width, MainSize.Height);            
        }

        public Size MainSize { get; }
        public Size MenuSize { get; }
        public Size PageSize { get; }
    }
}
