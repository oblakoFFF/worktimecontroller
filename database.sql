USE [master]
GO
/****** Object:  Database [timeWorkController]    Script Date: 27.03.2019 11:15:02 ******/
CREATE DATABASE [timeWorkController]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'timeWorkController', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\timeWorkController.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'timeWorkController_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\timeWorkController_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [timeWorkController] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [timeWorkController].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [timeWorkController] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [timeWorkController] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [timeWorkController] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [timeWorkController] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [timeWorkController] SET ARITHABORT OFF 
GO
ALTER DATABASE [timeWorkController] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [timeWorkController] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [timeWorkController] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [timeWorkController] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [timeWorkController] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [timeWorkController] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [timeWorkController] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [timeWorkController] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [timeWorkController] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [timeWorkController] SET  DISABLE_BROKER 
GO
ALTER DATABASE [timeWorkController] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [timeWorkController] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [timeWorkController] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [timeWorkController] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [timeWorkController] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [timeWorkController] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [timeWorkController] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [timeWorkController] SET RECOVERY FULL 
GO
ALTER DATABASE [timeWorkController] SET  MULTI_USER 
GO
ALTER DATABASE [timeWorkController] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [timeWorkController] SET DB_CHAINING OFF 
GO
ALTER DATABASE [timeWorkController] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [timeWorkController] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [timeWorkController] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'timeWorkController', N'ON'
GO
ALTER DATABASE [timeWorkController] SET QUERY_STORE = OFF
GO
USE [timeWorkController]
GO
/****** Object:  Table [dbo].[Admins]    Script Date: 27.03.2019 11:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admins](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[login] [nvarchar](20) NOT NULL,
	[password] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Должность]    Script Date: 27.03.2019 11:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Должность](
	[idДолжность] [int] IDENTITY(1,1) NOT NULL,
	[Наименование] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idДолжность] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Отдел]    Script Date: 27.03.2019 11:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Отдел](
	[idФилиала] [int] NOT NULL,
	[idОтдела] [int] IDENTITY(1,1) NOT NULL,
	[НаименованиеОтдела] [nvarchar](50) NOT NULL,
	[ВремяНачалаРаботыОтдела] [nvarchar](50) NOT NULL,
	[ВремяОкончаниеРаботыОтдела] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idОтдела] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ОтделыСотрудника]    Script Date: 27.03.2019 11:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ОтделыСотрудника](
	[idСотрудника] [int] NOT NULL,
	[idОтдела] [int] NOT NULL,
	[ВремяНачалаРаботыСотрудника] [date] NOT NULL,
	[ВремяОкончаниеРаботыСотрудника] [date] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Проход]    Script Date: 27.03.2019 11:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Проход](
	[idПрохода] [int] IDENTITY(1,1) NOT NULL,
	[Дата] [date] NOT NULL,
	[Статус] [nvarchar](5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idПрохода] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ПроходыСотрудника]    Script Date: 27.03.2019 11:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ПроходыСотрудника](
	[idСотрудника] [int] NOT NULL,
	[idПрохода] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Сотрудники]    Script Date: 27.03.2019 11:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Сотрудники](
	[idСотрудника] [int] IDENTITY(1,1) NOT NULL,
	[ФИО] [nvarchar](50) NOT NULL,
	[idДолжности] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idСотрудника] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Филиал]    Script Date: 27.03.2019 11:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Филиал](
	[idФилиал] [int] IDENTITY(1,1) NOT NULL,
	[Наименование] [nvarchar](50) NULL,
	[ВремяНачалоФилиала] [date] NOT NULL,
	[ВремяОкончаниеРаботыФилиала] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idФилиал] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Отдел]  WITH CHECK ADD FOREIGN KEY([idФилиала])
REFERENCES [dbo].[Филиал] ([idФилиал])
GO
ALTER TABLE [dbo].[ОтделыСотрудника]  WITH CHECK ADD FOREIGN KEY([idОтдела])
REFERENCES [dbo].[Отдел] ([idОтдела])
GO
ALTER TABLE [dbo].[ОтделыСотрудника]  WITH CHECK ADD FOREIGN KEY([idСотрудника])
REFERENCES [dbo].[Сотрудники] ([idСотрудника])
GO
ALTER TABLE [dbo].[ПроходыСотрудника]  WITH CHECK ADD FOREIGN KEY([idПрохода])
REFERENCES [dbo].[Проход] ([idПрохода])
GO
ALTER TABLE [dbo].[ПроходыСотрудника]  WITH CHECK ADD FOREIGN KEY([idСотрудника])
REFERENCES [dbo].[Сотрудники] ([idСотрудника])
GO
ALTER TABLE [dbo].[Сотрудники]  WITH CHECK ADD FOREIGN KEY([idДолжности])
REFERENCES [dbo].[Должность] ([idДолжность])
GO
/****** Object:  StoredProcedure [dbo].[ScalarQuery]    Script Date: 27.03.2019 11:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ScalarQuery]
AS
	SET NOCOUNT ON;
SELECT        COUNT(*) AS Expr1
FROM            Проход
GO
/****** Object:  StoredProcedure [dbo].[SelectПроходQuery]    Script Date: 27.03.2019 11:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SelectПроходQuery]
AS
	SET NOCOUNT ON;
SELECT idПрохода, Дата, Статус FROM dbo.Проход
GO
USE [master]
GO
ALTER DATABASE [timeWorkController] SET  READ_WRITE 
GO
